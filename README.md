# Tfeed

## Installation

Your env must contain your Twitter credentials:

```bash
TWITTER_CONSUMER_KEY
TWITTER_CONSUMER_SECRET
TWITTER_ACCESS_TOKEN
TWITTER_ACCESS_SECRET
```

If you use [envchain](https://github.com/sorah/envchain) here's a one-liner:
```bash
envchain --set twitter TWITTER_CONSUMER_KEY TWITTER_CONSUMER_SECRET TWITTER_ACCESS_TOKEN TWITTER_ACCESS_SECRET
```

## Usage

```
Fetches your Twitter timeline and shows only tweets which contains URLs.
Usage: tfeed [options]
    -f, --from FROM                  Start date to filter tweets. Defaults to 1 week ago
    -t, --to TO                      End date to filter tweets. Defaults to today
    -h, --help                       Show this help message
```

Example:

`bin/tfeed --from "1 week ago" --to "2 days ago"`

`to` and `from` can be any strings understood by [chronic](https://github.com/mojombo/chronic).

Note that Twitter limits API responses to 200 tweets per batch and 3,200 tweets per request window (15 mins),
so if your timeline has more than that amount in the given interval not all tweets will be displayed.