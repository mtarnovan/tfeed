# frozen_string_literal: true

require 'tfeed/version'
require 'tfeed/controller'
require 'optparse'
require 'chronic'

# Imagine that you don't want to or just don't have
# enough time to go through entire Twitter timeline
# every day and you are only interested in URLs to
# the various resources that are submitted by the
# people you follow (e.g., to blog posts).
# Implement a script that solves that problem and
# returns all tweets containing URLs.
# You should be able to specify either since when
# you want to fetch tweets or between what timestamps.
# Make it executable and runnable from console
# returning a nicely formatted result.
# The output should include the URLs themselves,
# date and some info indicating what it is about (especially for shortened URLs).

module Tfeed
  class CLI
    # rubocop:disable Metrics/MethodLength
    def initialize(options)
      @options = {}
      OptionParser.new do |opts|
        opts.banner = <<~DOC
          Fetches your Twitter timeline and shows only tweets which contains URLs.
          Usage: tfeed [options]
        DOC
        opts.accept(DateTime) do |datetime_string|
          Chronic.parse(datetime_string)
        end
        opts.on('-f', '--from FROM', DateTime, 'Start date to filter tweets. Defaults to 1 week ago') do |v|
          @options[:from] = v
        end
        opts.on('-t', '--to TO', DateTime, 'End date to filter tweets. Defaults to today') do |v|
          @options[:to] = v
        end
        opts.on('-h', '--help', 'Show this help message') do
          puts opts
          exit!
        end
      end.parse!(options)

      prepare_options
    end
    # rubocop:enable Metrics/MethodLength

    def run
      puts @options
      Controller.new(@options).run
    end

    private

    def prepare_options
      @options[:from] ||= Chronic.parse('1 week ago')
      @options[:to] ||= Chronic.parse('now')
      return if @options[:from] < @options[:to]

      abort("Invalid arguments: 'from' #{@options[:from]} must be before 'to' #{@options[:to]}.")
    end
  end
end
