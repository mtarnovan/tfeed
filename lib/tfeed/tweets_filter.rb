# frozen_string_literal: true

module Tfeed
  # Filter tweets outside `from` - `to`
  # and tweets that only contain one URI (the self-link)
  class TweetsFilter
    def initialize(tweets, from:, to:)
      @tweets = tweets
      @from = from
      @to = to
    end

    def filter
      @tweets.select { |t| in_date_range_with_links?(t) }
    end

    private

    def in_date_range_with_links?(tweet)
      tweet.created_at.between?(@from, @to) && tweet.uris.size <= 1
    end
  end
end
