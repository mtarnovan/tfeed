# frozen_string_literal: true

require 'terminal-table'

module Tfeed
  class TableFormatter
    def initialize(tweets_with_link_titles)
      @tweets = tweets_with_link_titles
    end

    def to_table
      Terminal::Table.new do |t|
        t.add_row %w[Date Link Title]
        t.add_separator
        @tweets.each do |tweet, uris_with_titles|
          uris_with_titles.each do |uri_with_title|
            row = [format_time(tweet.created_at), format_link(uri_with_title.uri), format_title(uri_with_title.title)]
            t.add_row(row)
          end
        end
      end
    end

    private

    def format_link(uri)
      uri.to_s[0...30] + '…'
    end

    def format_title(title)
      title[0...80].tr("\n", "\t").squeeze(' ').strip + '…'
    end

    def format_time(time)
      time.getlocal.strftime('%d.%b %H:%M:%S')
    end
  end
end
