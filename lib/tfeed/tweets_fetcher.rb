# frozen_string_literal: true

require 'twitter'
require 'pry'

module Tfeed
  class TweetsFetcher
    DEFAULT_FETCH_OPTIONS = {
      count: Twitter::REST::Timelines::MAX_TWEETS_PER_REQUEST,
      trim_user: true,
      exclude_replies: true,
      include_rts: false
    }
    def initialize
      @client = Twitter::REST::Client.new do |config|
        config.consumer_key        = ENV['TWITTER_CONSUMER_KEY']
        config.consumer_secret     = ENV['TWITTER_CONSUMER_SECRET']
        config.access_token        = ENV['TWITTER_ACCESS_TOKEN']
        config.access_token_secret = ENV['TWITTER_ACCESS_SECRET']
      end
    end

    def fetch_tweets(from:)
      collect_with_max_id(from) do |max_id|
        options = DEFAULT_FETCH_OPTIONS.tap do |hash|
          hash[:max_id] = max_id unless max_id.nil?
        end
        @client.home_timeline(options)
      end
    rescue Twitter::Error::TooManyRequests => e
      puts "Rate limit exceeded, sleeping #{e.rate_limit.reset_in}s"
      sleep e.rate_limit.reset_in + 1
      retry
    end

    def collect_with_max_id(from, collection = [], max_id = nil, &block)
      response = yield(max_id)
      collection += response
      puts "Fetched a batch of #{response.size} tweets"
      if response.empty? || (response.last.created_at < from)
        collection.flatten
      else
        collect_with_max_id(from, collection, response.last.id - 1, &block)
      end
    end
  end
end
