# frozen_string_literal: true

require 'tfeed/tweets_fetcher'
require 'tfeed/tweets_filter'
require 'tfeed/link_fetcher'
require 'tfeed/table_formatter'

module Tfeed
  class Controller
    def initialize(options)
      @options = options
    end

    def run
      tweets = TweetsFetcher.new.fetch_tweets(from: @options[:from])
      tweets = TweetsFilter.new(tweets, from: @options[:from], to: @options[:to]).filter
      if tweets.empty?
        puts 'No tweets with links in the given interval'
        return
      end

      tweets_with_link_titles = LinkFetcher.new(tweets).fetch
      puts TableFormatter.new(tweets_with_link_titles).to_table
    end
  end
end
