# frozen_string_literal: true

require 'uri'
require 'typhoeus'
require 'nokogiri'

module Tfeed
  # Given a collection of tweets, return with tweets as keys
  # and an array of URIWithTitle objects as values
  class LinkFetcher
    # All tweets contain a self-referential link, use this string
    # to filter them out.
    SELF_URI_START = 'https://twitter.com/i/web/status/'
    UNABLE_TO_FETCH_LINK = '(unable to fetch this link)'

    class URIWithTitle
      attr_accessor :uri, :title
      def initialize(uri:, title:)
        @uri = uri
        @title = title
      end
    end

    def initialize(tweets)
      @tweets = tweets
    end

    def fetch
      # extract urls from tweets, ignoring self-links
      tmp = @tweets.map { |t| [t, t.uris.map(&:expanded_url).reject { |uri| self_uri?(uri) }] }
                   .reject { |t| t.last.empty? }
      filtered_tweets = Hash[tmp]
      puts "Found #{filtered_tweets.size} tweets with links in the given interval, fetching link titles...\n"

      hydra = Typhoeus::Hydra.new
      tweets_with_requests = build_requests(filtered_tweets, hydra)
      hydra.run

      extract_titles(tweets_with_requests)
    end

    private

    def build_requests(tweets, hydra)
      Hash[tweets.map do |t, urls|
        requests = []
        urls.each do |url|
          request = Typhoeus::Request.new(url, followlocation: true)
          requests << request
          hydra.queue(request)
        end
        [t, requests]
      end]
    end

    def extract_titles(tweets_with_requests)
      Hash[
        tweets_with_requests.map do |t, requests|
          uris_with_titles = requests.map do |request|
            URIWithTitle.new(uri: request.url, title: extract_title(request))
          end
          [t, uris_with_titles]
        end
      ]
    end

    def extract_title(request)
      return UNABLE_TO_FETCH_LINK if (html = request.response.body).empty?

      Nokogiri::HTML(html).css('title').text
    rescue StandardError => e
      "Could not extract a title #{e}"
    end

    def self_uri?(uri)
      uri.to_s.start_with?(SELF_URI_START)
    end
  end
end
